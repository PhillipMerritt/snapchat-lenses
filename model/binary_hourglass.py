"""
    My attempt at binarizing an existing stacked HG model to get a performance boost.
    Theory: https://arxiv.org/pdf/1703.00862.pdf
    original stacked HG code: https://github.com/ethanyanjiali/deep-vision/blob/master/Hourglass/tensorflow/hourglass104.py
    binary tensorflow layers: https://github.com/DingKe/nn_playground/tree/master/binarynet
"""

from binarynet.binary_layers import BinaryConv2D, Clip
from binarynet.binary_ops import binary_tanh, binarize, binary_sigmoid
import tensorflow as tf
from tensorflow.keras.layers import Input, Conv2D, Add, BatchNormalization, MaxPool2D, Concatenate, UpSampling2D

kernel_lr_multiplier = 'Glorot'
use_bias = False

def BottleneckBlock(inputs, filters, strides=1, downsample=False, name=None):
    """
    "Our ﬁnal design makes extensive use of residual modules. Filters greater than 3x3 are never used,
    and the bottlenecking restricts the total number of parameters at each layer curtailing total
    memory usage. The module used in our network is shown in Figure 4." [1]
    Ethan: can't tell what's the exact structure, so we need to refer to his source code here:
    https://github.com/princeton-vl/pose-hg-train/blob/master/src/models/layers/Residual.lua
    this follows ResNet V1 but also puts BN ahead which is used in ResNet V2
    """
    identity = inputs
    if downsample:
        identity = Conv2D(
            filters=filters,  # lift channels first
            kernel_size=1,
            strides=strides,
            padding='same',
            kernel_initializer='he_normal')(inputs)

    x = BatchNormalization(momentum=0.9)(inputs)
    x = ReLU()(x)
    x = Conv2D(
        filters=filters // 2,
        kernel_size=1,
        strides=1,
        padding='same',
        kernel_initializer='he_normal')(x)

    x = BatchNormalization(momentum=0.9)(x)
    x = ReLU()(x)
    x = Conv2D(
        filters=filters // 2,
        kernel_size=3,
        strides=strides,
        padding='same',
        kernel_initializer='he_normal')(x)

    x = BatchNormalization(momentum=0.9)(x)
    x = ReLU()(x)
    x = Conv2D(
        filters=filters,
        kernel_size=1,
        strides=1,
        padding='same',
        kernel_initializer='he_normal')(x)

    x = Add()([identity, x])
    return x

def binary_bottleneck(inputs, filters, strides=1, downsample=False, name=None):
    identity = inputs
    if downsample:
        identity = BinaryConv2D(filters, kernel_size=1, strides=strides,
                        kernel_lr_multiplier=kernel_lr_multiplier, 
                        padding='same', use_bias=use_bias)(inputs)

    x = BatchNormalization(momentum=0.9)(inputs)
    x = binary_tanh(x)
    identity2 = BinaryConv2D(filters // 2, kernel_size=3, strides = 1, padding='same', use_bias=use_bias)(x)

    x = BatchNormalization(momentum=0.9)(identity2)
    x = binary_tanh(x)
    identity3 = BinaryConv2D(filters // 4, kernel_size=3, strides=strides, padding='same', use_bias=use_bias)(x)

    x = BatchNormalization(momentum=0.9)(identity3)
    x = binary_tanh(x)
    identity4 = BinaryConv2D(filters // 4, kernel_size=3, strides=1, padding='same', use_bias=use_bias)(x)

    conc = Concatenate()([identity2, identity3, identity4])
    x = Add()([identity, conc])
    return binarize(x)

def binary_hourglass(inputs, order, filters, num_residual):
    """
    https://github.com/princeton-vl/pose-hg-train/blob/master/src/models/hg.lua#L3
    """
    # Upper branch
    up1 = binary_bottleneck(inputs, filters, downsample=False)

    for i in range(num_residual):
        up1 = binary_bottleneck(up1, filters, downsample=False)

    # Lower branch
    low1 = MaxPool2D(pool_size=2, strides=2)(inputs)
    for i in range(num_residual):
        low1 = binary_bottleneck(low1, filters, downsample=False)

    low2 = low1
    if order > 1:
        low2 = binary_hourglass(low1, order - 1, filters, num_residual)
    else:
        for i in range(num_residual):
            low2 = binary_bottleneck(low2, filters, downsample=False)

    low3 = low2
    for i in range(num_residual):
        low3 = binary_bottleneck(low3, filters, downsample=False)

    up2 = UpSampling2D(size=2)(low3)

    return binarize(up2 + up1)

def binary_linear_layer(inputs, filters):
    x = BinaryConv2D(filters, kernel_size=1, strides=1,
                    kernel_lr_multiplier=kernel_lr_multiplier, 
                    padding='same', use_bias=use_bias)(inputs)
    x = BatchNormalization(momentum=0.9)(x)
    x = binary_tanh(x)
    return x


def binary_stacked_hourglass(input_shape=(256, 256, 3), num_stack=4, num_residual=1,
        num_heatmap=68):
    inputs = Input(shape=input_shape)
    x = Conv2D(
        filters=64,
        kernel_size=7,
        strides=2,
        padding='same',
        kernel_initializer='he_normal')(inputs)
    x = tf.keras.layers.BatchNormalization(momentum=0.9)(x)
    x = tf.keras.layers.ReLU()(x)
    x = binary_bottleneck(x, 128, downsample=True, name='BN1')
    x = MaxPool2D(pool_size=2, strides=2)(x)
    x = binary_bottleneck(x, 128, downsample=False, name='BN2')
    x = binary_bottleneck(x, 256, downsample=True, name='BN3')

    outputs = []
    for i in range(num_stack):
        x = binary_hourglass(x, order=4, filters=256, num_residual=num_residual)
        for j in range(num_residual):
            x = binary_bottleneck(x, 256, downsample=False)

        x = binary_linear_layer(x, 256)

        y = Conv2D(filters=num_heatmap,
            kernel_size=1,
            strides=1,
            padding='same',
            kernel_initializer='he_normal')(x)
        outputs.append(y)

        if i < num_stack - 1:
            y_intermediate_1 = BinaryConv2D(filters=256, kernel_size=1, strides=1)(x)
            y_intermediate_2 = BinaryConv2D(filters=256, kernel_size=1, strides=1)(y)
            x = Add()([y_intermediate_1, y_intermediate_2])
            x = binarize(x)

    return tf.keras.Model(inputs, outputs)