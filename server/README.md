# SnapchatLenses (server)

WebRTC signaling server in FastAPI, based on [this](https://www.tutorialspoint.com/webrtc/webrtc_signaling.htm).

Read about FastAPI WebSockets [here](https://fastapi.tiangolo.com/advanced/websockets/).

## Install the dependencies

Requires [poetry](https://python-poetry.org/).

```bash
poetry install
```

## Activate project shell

Note that ipython and pytest are both available as dev dependencies.

```bash
poetry shell
```

## Run the dev server

```bash
poetry run uvicorn main:app --reload
```

## Configure VS Code

Add the following to your settings.json:

```json
{
  "python.pythonPath": "~/.cache/pypoetry/virtualenvs/${YOUR_POETRY_ENV_NAME}/bin/python"
}
```
