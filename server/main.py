from fastapi import (
    FastAPI,
    Request,
    WebSocket,
    WebSocketDisconnect
)
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

# Websocks: https://fastapi.tiangolo.com/advanced/websocks/
# Templates and static files: https://fastapi.tiangolo.com/advanced/templates/

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")


class WebSocketConnectionPool:
    # WebSocket.send_json and WebSocket.receive_json
    # both understand Python dictionaries
    def __init__(self):
        self.active_connections: Dict[int, WebSocket] = {}
        self.users_ids: Dict[str, int] = {}
        self.client_names: Dict[int, str] = {}
        self.user_connections: Dict[str, str] = {}

    def has_user(self, n: str):
        return n in self.users_ids

    def get_user(self, n: str):
        return self.users_ids[n]

    def has_client(self, client_id: str):
        return client_id in self.client_names

    def get_client(self, client_id: str):
        return self.client_names[client_id]

    def register_user(self, client_id: int, n: str):
        self.users_ids[n] = client_id
        self.client_names[client_id] = n

    def connect_users(self, n1: str, n2: str):
        self.user_connections[n1] = n2
        self.user_connections[n2] = n1

    def is_user_connected(self, n: str):
        return n in self.user_connections

    def get_user_connected_to(self, n: str):
        return self.user_connections[n]

    # only need one user to disconnect
    def disconnect_user(self, n1: str):
        n2 = self.user_connections[n1]
        del self.user_connections[n1]
        del self.user_connections[n2]

    async def connect(self, client_id: int, websocket: WebSocket):
        if client_id not in self.active_connections:
            await websocket.accept()
            self.active_connections[client_id] = websocket

    def close(self, client_id: int):
        del self.active_connections[client_id]

    async def send_to(self, client_id: int, message: dict):
        await self.active_connections[client_id].send_json(message)

    async def recv_from(self, client_id: int):
        data = await self.active_connections[client_id].receive_json()
        return data

    async def broadcast(self, message: dict):
        for client_id in self.active_connections:
            await self.active_connections[client_id].send_json(message)


socks = WebSocketConnectionPool()


@app.get("/", response_class=HTMLResponse)
async def index(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})


# based on node signaling server from this tutorial:
# https://www.tutorialspoint.com/webrtc/webrtc_video_demo.htm
@app.websocket("/webrtc/{client_id}")
async def webrtc(websocket: WebSocket, client_id: int):
    await socks.connect(client_id, websocket)
    try:
        await socks.send_to(client_id, {
            "type": "hello"
        })
        while True:
            data = await socks.recv_from(client_id)
            t = data['type']

            if t == "login":
                n = data['name']
                print(f"Attempted user login: {n}")

                # don't allow if already logged in
                if socks.has_user(n):
                    socks.send_to(client_id, {
                        "type": "login",
                        "success": False
                    })
                else:
                    socks.register_user(client_id, n)
                    await socks.send_to(client_id, {
                        "type": "login",
                        "success": True
                    })

            elif t == "offer":
                n = data['name']
                n2 = socks.get_client(client_id)
                print(f"{n2} sending offer to {n}")

                if socks.has_user(n):
                    if not socks.is_user_connected(n):
                        socks.connect_users(n, n2)
                    await socks.send_to(
                        socks.get_user(n), {
                            "type": "offer",
                            "offer": data['offer'],
                            "name": n2
                        })

            elif t == "answer":
                n = data['name']
                n2 = socks.get_client(client_id)
                print(f"{n2} sending answer to {n}")

                if socks.has_user(n):
                    if not socks.is_user_connected(n):
                        socks.connect_users(n, n2)
                    await socks.send_to(
                        socks.get_user(n), {
                            "type": "answer",
                            "answer": data['answer']
                        }
                    )

            elif t == "candidate":
                n = data['name']
                n2 = socks.get_client(client_id)
                print(f"{n2} sending candidate to {n}")

                if socks.has_user(n):
                    await socks.send_to(
                        socks.get_user(n), {
                            "type": "candidate",
                            "candidate": data['candidate']
                        }
                    )

            elif t == "leave":
                n = data['name']
                print(f"{n} is leaving")

                if socks.is_user_connected(n):
                    n2 = socks.get_user_connected_to(n)
                    socks.disconnect_user(n)
                    await socks.send_to(
                        socks.get_user(n2), {
                            "type": "leave"
                        }
                    )

            else:
                await socks.send_to(
                    client_id, {
                        "type": "error",
                        "message": f"command not found: {t}"
                    }
                )

    except WebSocketDisconnect:
        n = socks.get_client(client_id)
        if socks.is_user_connected(client_id):
            n2 = socks.get_user_connected_to(n)
            socks.disconnect_user(n)
            await socks.send_to(
                socks.get_user(n2), {
                    "type": "leave"
                }
            )
        socks.close(client_id)
        print(f"{client_id} has disconnected")
