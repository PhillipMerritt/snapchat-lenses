# SnapchatLenses (webapp)

Playing with computer vision to do image filtering and real-time VFX.

Cutout of sunglasses borrowed from the following MIT-licensed project: https://github.com/acl21/Selfie_Filters_OpenCV

## Install the dependencies
```bash
yarn && npm i
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
