import { boot } from "quasar/wrappers";
import { readAmLoggedIn } from "src/store/user/getters";

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/cli-documentation/boot-files#Anatomy-of-a-boot-file
export default boot(async ({ router, store }) => {
  router.beforeEach((to, from, next) => {
    if (readAmLoggedIn(store)) {
      if (to.path === "/" || to.path === "") {
        next("/chat");
      } else {
        next();
      }
    } else {
      next();
    }
  });
});
