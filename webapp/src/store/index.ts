import { store } from 'quasar/wrappers'
import Vuex, { StoreOptions } from 'vuex'
import { State } from './state'
import { mediaModule } from './media'
import { userModule } from './user'
import { webSocketModule } from './websocket'
import { webRTCModule } from './webrtc'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default store(function ({ Vue }) {
  Vue.use(Vuex)

  const storeOptions: StoreOptions<State> = {
    modules: {
      media: mediaModule,
      user: userModule,
      websocket: webSocketModule,
      webrtc: webRTCModule,
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: !!process.env.DEV
  }

  const Store = new Vuex.Store<State>(storeOptions)

  return Store
})
