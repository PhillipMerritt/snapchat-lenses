import { MediaState, DrawContext, FilterImageInfo } from "./state";
import { State } from "../state";
import { readActiveFilters, readKeyPointsForImage } from "./getters";
import {
  commitSetActivelyDrawing,
  commitSetKpMod,
  commitSetFilterImage
} from "./mutations";
import { getStoreAccessors } from "typesafe-vuex";
import { ActionContext } from "vuex";
import { loadGraphModel } from "@tensorflow/tfjs";

type MediaContext = ActionContext<MediaState, State>;

export const actions = {
  actionNextFrame: async (context: MediaContext, payload: DrawContext) => {
    payload = await dispatchProcessImage(context, payload);
    if (context.state.isActivelyDrawing)
      requestAnimationFrame(() => dispatchNextFrame(context, payload));
  },
  actionStartDrawing: async (context: MediaContext, payload: DrawContext) => {
    commitSetActivelyDrawing(context, true);
    dispatchNextFrame(context, payload);
  },
  actionStopDrawing: async (context: MediaContext) => {
    commitSetActivelyDrawing(context, false);
  },
  actionLoadKeyPointModel: async (context: MediaContext) => {
    if (!context.state.kpmod) {
      const kpmod = await loadGraphModel(
        "https://raw.githubusercontent.com/PhillipMerritt/keypoint_model/master/model.json",
        { strict: true }
      );
      commitSetKpMod(context, kpmod);
    }
  },
  actionProcessImage: async (
    context: MediaContext,
    payload: DrawContext
  ): Promise<DrawContext> => {
    // based on this:
    // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Manipulating_video_using_canvas
    payload.ctx.drawImage(payload.img, 0, 0, payload.width, payload.height);
    payload.frame = payload.ctx.getImageData(
      0,
      0,
      payload.width,
      payload.height
    );

    // readActiveFilters returns filters that have active
    // set to true and sorts them by draw order
    const filters = readActiveFilters(context);
    if (filters.some(f => f.requiresKeyPoints)) {
      payload.kp = {
        points: readKeyPointsForImage(context)(payload.frame),
        max_eye: 0,
        max_nose: 0
      };
    }

    for (let f of filters) {
      if (!f.requiresKeyPoints || (payload.kp && payload.kp.points.length)) {
        // load filter image if needed, then save back to store
        if (f.finfo.needsToLoad) {
          const img = await f.finfo.load();
          commitSetFilterImage(context, { name: f.name, img: img });
        }
        // swap out filter states on drawing context
        payload.finfo = f.finfo;
        payload = f.draw(payload);
      }
      payload.frame = payload.ctx.getImageData(
        0,
        0,
        payload.width,
        payload.height
      );
    }
    payload.finfo = undefined;
    return payload;
  }
};

const { dispatch } = getStoreAccessors<MediaState | any, State>("");

export const dispatchStartDrawing = dispatch(actions.actionStartDrawing);
export const dispatchStopDrawing = dispatch(actions.actionStopDrawing);
export const dispatchLoadKeyPointModel = dispatch(
  actions.actionLoadKeyPointModel
);
export const dispatchNextFrame = dispatch(actions.actionNextFrame);
export const dispatchProcessImage = dispatch(actions.actionProcessImage);
