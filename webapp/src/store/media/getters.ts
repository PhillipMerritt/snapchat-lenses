import { MediaState } from "./state";
import { getStoreAccessors } from "typesafe-vuex";
import { Filter, TrackSets } from "./state";
import { State } from "../state";
import { heatmapsToKeyPoints } from "./scripts/drawing";
import * as tf from "@tensorflow/tfjs";

export const getters = {
  isActivelyDrawing: (state: MediaState) => state.isActivelyDrawing,
  filters: (state: MediaState) => state.filters,
  activeFilters: (state: MediaState): Array<Filter> => {
    return state.filters
      .filter(f => f.active)
      .sort((f1, f2) => f1.drawOrder - f2.drawOrder);
  },
  mediaTracks: (state: MediaState) => state.mediaTracks,
  mediaTrackSet: (state: MediaState) => (payload: TrackSets) =>
    state.mediaTracks[payload],
  keyPointModel: (state: MediaState) => state.kpmod,
  exposedAudioTracks: (state: MediaState) => {
    state.localMediaTracks.filter(t => t.kind == "audio");
  },
  exposedVideoTracks: (state: MediaState) => {
    state.localMediaTracks.filter(t => t.kind == "video");
  },
  keyPointsForImg: (state: MediaState) => (
    img: ImageData
  ): Array<Array<number>> => {
    const crop_amt = (img.width - img.height) / (2 * img.width);
    const tensor_input = tf.browser.fromPixels(img, 3).expandDims();
    let resized_img = tf.image.cropAndResize(
      tensor_input,
      [[0, crop_amt, 1, 1 - crop_amt]],
      [0],
      [256, 256]
    );

    resized_img = resized_img.mul(tf.scalar(1 / 255));

    if (state.kpmod) {
      const pred = state.kpmod.execute(resized_img);
      //const arr = Array.from(pred);
      //console.log(pred);
      const arr = pred[pred.length - 1].arraySync();
      const coords = heatmapsToKeyPoints(arr[0]);
      //console.log(coords);
      return coords;
    }

    return [];
  }
};

const { read } = getStoreAccessors<MediaState, State>("");

export const readIsActivelyDrawing = read(getters.filters);
export const readFilters = read(getters.filters);
export const readActiveFilters = read(getters.activeFilters);
export const readMediaTracks = read(getters.mediaTracks);
export const readMediaTrackSet = read(getters.mediaTrackSet);
export const readExposedAudioTracks = read(getters.exposedAudioTracks);
export const readExposedVideoTracks = read(getters.exposedVideoTracks);
export const readKeyPointModel = read(getters.keyPointModel);
export const readKeyPointsForImage = read(getters.keyPointsForImg);
