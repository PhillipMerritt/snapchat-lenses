import { mutations } from "./mutations";
import { getters } from "./getters";
import { actions } from "./actions";
import { MediaState } from "./state";
import {
  drawKeyPoints,
  drawRedBox,
  drawShades,
  drawNegated,
  loadImage
} from "./scripts/drawing";

const defaultFilterImageInfo = {
  img: null,
  needsToLoad: false,
  load: async () => null
};

const defaultMediaState: MediaState = {
  isActivelyDrawing: false,
  filters: [
    {
      name: "keypoints",
      active: false,
      drawOrder: 1,
      requiresKeyPoints: true,
      draw: drawKeyPoints,
      finfo: defaultFilterImageInfo
    },
    {
      name: "shades",
      active: false,
      drawOrder: 2,
      requiresKeyPoints: true,
      draw: drawShades,
      finfo: {
        img: null,
        needsToLoad: true,
        load: async () => {
          /* this is mostly just to show how you might load an image overlay
            into the filter system, but there are several issues. a few are
            documented in comments in the loadImage function. another is that
            you'd normally want to have a dedicated static file server for your
            project pre-loaded with all the images you'd want to use, which gives
            you the opportunity to do things like use an external image editor to
            make the backgrounds transparent, which the current setup doesn't support. */
          return loadImage(
            "https://raw.githubusercontent.com/acl21/Selfie_Filters_OpenCV/master/images/sunglasses.png"
          );
        }
      }
    },
    {
      name: "red box",
      active: false,
      drawOrder: 3,
      requiresKeyPoints: true,
      draw: drawRedBox,
      finfo: {
        img: null,
        needsToLoad: true,
        load: async () => {
          const tmp_canvas = document.createElement("canvas");
          tmp_canvas.width = 150;
          tmp_canvas.height = 50;

          let tmp_ctx = tmp_canvas.getContext("2d") as CanvasRenderingContext2D;
          tmp_ctx.fillStyle = "#FF0000";
          tmp_ctx.fillRect(0, 0, 150, 50);

          return tmp_ctx.getImageData(0, 0, 150, 50);
        }
      }
    },
    {
      name: "negative",
      active: false,
      drawOrder: 4,
      requiresKeyPoints: false,
      draw: drawNegated,
      finfo: defaultFilterImageInfo
    }
  ],
  kpmod: null,
  mediaTracks: {
    Local: new Set(),
    Remote: new Set()
  }
};

export const mediaModule = {
  state: defaultMediaState,
  mutations,
  getters,
  actions
};
