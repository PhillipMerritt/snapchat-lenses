import { MediaState, Filter, TrackSets } from "./state";
import { getStoreAccessors } from "typesafe-vuex";
import { State } from "../state";
import { GraphModel } from "@tensorflow/tfjs";

export const mutations = {
  setActivelyDrawing(state: MediaState, payload: boolean) {
    state.isActivelyDrawing = payload;
  },
  setKpMod(state: MediaState, payload: GraphModel) {
    state.kpmod = payload;
  },
  addMediaTrackToSet(
    state: MediaState,
    payload: { source: TrackSets; track: MediaStreamTrack }
  ) {
    state.mediaTracks[payload.source].add(payload.track);
  },
  setMediaTracks(
    state: MediaState,
    payload: { source: TrackSets; tracks: Iterable<MediaStreamTrack> }
  ) {
    state.mediaTracks[payload.source] = new Set(payload.tracks);
  },
  setFilterImage(
    state: MediaState,
    payload: { name: string; img: ImageData | null }
  ) {
    if (payload.img) {
      const i = state.filters.findIndex(f => f.name === payload.name);
      if (i !== -1) {
        state.filters[i].finfo.img = payload.img;
        state.filters[i].finfo.needsToLoad = false;
      }
    }
  },
  toggleFilterActive(state: MediaState, payload: string) {
    const i = state.filters.findIndex(f => f.name === payload);
    if (i !== -1) state.filters[i].active = !state.filters[i].active;
  },
  setFiltersActive(state: MediaState, payload: Array<string>) {
    state.filters.forEach(f => {
      if (payload.includes(f.name)) f.active = true;
      else f.active = false;
    });
  },
  upsertFilter(state: MediaState, payload: Filter) {
    const i = state.filters.findIndex(f => f.name === payload.name);
    if (i !== -1) state.filters[i] = payload;
    else state.filters.push(payload);
  }
};

const { commit } = getStoreAccessors<MediaState | any, State>("");

export const commitSetActivelyDrawing = commit(mutations.setActivelyDrawing);
export const commitAddMediaTrackToSet = commit(mutations.addMediaTrackToSet);
export const commitSetMediaTracks = commit(mutations.setMediaTracks);
export const commitSetKpMod = commit(mutations.setKpMod);
export const commitSetFilterImage = commit(mutations.setFilterImage);
export const commitToggleFilterActive = commit(mutations.toggleFilterActive);
export const commitSetFiltersActive = commit(mutations.setFiltersActive);
export const commitUpsertFilter = commit(mutations.upsertFilter);
