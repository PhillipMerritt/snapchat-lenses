import { MutationPayload } from "vuex";
import { TrackSets } from "../state";

// helper functions for attaching MediaStreams to HTMLVideoElements.
// to blank out a video element, use attachMediaTracks(vid, []).

export const attachMediaTrack = (
  target: HTMLVideoElement,
  track: MediaStreamTrack
) => {
  if (!target.srcObject) target.srcObject = new MediaStream();
  target.srcObject.addTrack(track);
};

export const attachMediaTracks = (
  target: HTMLVideoElement,
  tracks: Iterable<MediaStreamTrack>
) => {
  if (!Array.from(tracks).length) target.srcObject = null;
  else {
    for (let t of tracks) attachMediaTrack(target, t);
  }
};

/* 
  my attempt at modular media subscriptions, with as few
  hard-coded strings and template strings as possible.
  
  with a code example from a version of Login.vue:
  this.unsubscribe = this.$store.subscribe(
      createMediaSubscriptions([
        { target: this.localVideoLogin, source: TrackSets.local },
      ])
    );
  
  this creates a Vuex subscription and stores the unsubscribe
  function in the component instance. when that code is executed,
  any time the addMediaTrackToSet or setMediaTracks mutations
  are run against with the specified source in their payload, the
  new media tracks are attached to the localVideoLogin element.

  the point of this is partly to make it hard to screw up by subscribing 
  to something that doesn't exist, i.e. there probably aren't many situations
  where it will fail silently on you. the other part is to make it possible
  for a caller to access media streams by describing what they want rather
  than specificying what code needs to execute.

  i don't like the hard-coded strings, but at least if you want to change
  a mutation name or payload definition then this code will keep working 
  if you use your IDE to "replace all in project" or something like that.
*/

export const createMediaSubscriptions = (
  subs: Array<{ target: HTMLVideoElement; source: TrackSets }>
) => (mut: MutationPayload) => {
  for (let s of subs) {
    try {
      if (
        mut.payload &&
        mut.payload.source &&
        mut.payload.source === s.source
      ) {
        if (mut.type === "addMediaTrackToSet") {
          attachMediaTrack(s.target, mut.payload.track);
        } else if (mut.type === "setMediaTracks") {
          attachMediaTracks(s.target, mut.payload.tracks);
        }
      }
    } catch (e) {
      console.log(e);
    }
  }
};
