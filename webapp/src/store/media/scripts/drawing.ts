import { DrawContext, KeyPointTransform, FilterImageInfo } from "../state";

// calculates the 3D rotation of the face given the keypoints
// returns these values in the form of CSS's rotate3d()
// TODO: implement z rotation
export const rotateFilter = (d: DrawContext): DrawContext => {
  if (d.kp) {
    // euclidean distance between outer eye points and between top of nose and top of mouth
    const eye_distance = Math.sqrt(
      Math.pow(d.kp.points[36][0] - d.kp.points[45][0], 2) +
        Math.pow(d.kp.points[36][1] - d.kp.points[45][1], 2)
    );
    const nose_distance = Math.sqrt(
      Math.pow(d.kp.points[27][0] - d.kp.points[51][0], 2) +
        Math.pow(d.kp.points[27][1] - d.kp.points[51][1], 2)
    );

    const max_delta = 10;

    // keep track of the maximum eye and nose distance
    // there is a certain angle where the model spazzes out so I don't record changes greater than max_delta
    if (
      eye_distance > d.kp.max_eye &&
      (eye_distance - d.kp.max_eye < max_delta || d.kp.max_eye === 0)
    )
      d.kp.max_eye = eye_distance;

    if (
      nose_distance > d.kp.max_nose &&
      (nose_distance - d.kp.max_nose < max_delta || d.kp.max_nose === 0)
    )
      d.kp.max_nose = nose_distance;

    // const eye_ratio = eye_distance / d.kp.max_eye; // ratio of current eye distance over the max
    // const nose_ratio = nose_distance / d.kp.max_nose; // same for nose
    const flat_ratio_y = d.kp.max_eye / d.kp.max_nose; // the ratios between the max nose and eye distances
    const flat_ratio_x = d.kp.max_nose / d.kp.max_eye;
    // the ratios between the current nose and eye distances
    const ratio_y = eye_distance / nose_distance; // ratio_y approaches zero as you rotate about the y-axis (looking left or right)
    const ratio_x = nose_distance / eye_distance; // ratio_x approaches zero as you rotate about the x-axis (looking up or down)

    // these are linear functions to find the rotation
    // finding y rotation ex:
    // let the domain be the possible ratio_y values 0<=x<inf, let the range be the degrees of rotation 0<=y<=90
    // to find y = mx + b use the points (0,90) and (flat_ratio_y, 0)
    // these come from x (ratio_y) being zero when the head is rotated 90 degrees and x=ratio_y=flat_ratio_y when the y rotation is 0 degrees
    // m = (y2 - y1) / (x2 - x1) = (0 - 90) / (flat_ratio_y - 0) = -90 / flat_ratio_y
    // b = 90 always
    // thus y_rotation = (-90 / flat_ratio_y) * ratio_y + 90
    // This will give negative values when rotating about the x-axis only so I limit the value to a minimum of 0
    let m = -90 / flat_ratio_y;
    let y_rotation = Math.max(0, m * ratio_y + 90);
    //let y_rotation = Math.max(0, Math.min(m * ratio_y + 90, 90)) * Math.min(1, Math.max(nose_ratio / eye_ratio, 0));

    // same for x_rotation but using the x ratios instead
    m = -90 / flat_ratio_x;
    let x_rotation = Math.max(0, m * ratio_x + 90);
    //let x_rotation = Math.max(0, Math.min(m * ratio_x + 90, 90)) * Math.min(1, Math.max(eye_ratio / nose_ratio, 0));

    // convert the rotations to CSS rotate3d() format
    let degree = 0;
    if (x_rotation > y_rotation) {
      degree = x_rotation;
      y_rotation = y_rotation / x_rotation;
      x_rotation = 1;
    } else {
      degree = y_rotation;
      x_rotation = x_rotation / y_rotation;
      y_rotation = 1;
    }

    d.kp.rotations = {
      x: x_rotation,
      y: y_rotation,
      zero: 0,
      degree: degree
    };
  }

  return d;
};

// calls rotateFilter
// creates a copy of ogImage with the rotations applied
// returns the transformed filter and the upper left coordinate of where it should go
export const transformFilter = (d: DrawContext): DrawContext => {
  // get rotations
  d = rotateFilter(d);
  if (d.kp && d.finfo && d.finfo.img) {
    // apply rotations
    // TODO: figure out how to rotate the filter and draw it to canvas

    // middle of eyes
    const midpoint = [
      Math.round((d.kp.points[36][0] + d.kp.points[45][0]) / 2),
      Math.round((d.kp.points[36][1] + d.kp.points[45][1]) / 2)
    ];

    // find upper left xy coord
    const left_x = Math.round(midpoint[0] - d.finfo.img.width / 2);
    const upper_y = Math.round(
      // stopped using vOffset when i realized that it was always zero,
      // but here's where it would go if it was ever needed again:
      // midpoint[1] - f.vOffset - d.finfo.img.height / 2
      midpoint[1] - d.finfo.img.height / 2
    );

    // return image transform
    d.kp.transform = {
      left_x: left_x,
      upper_y: upper_y
    };
  }

  return d;
};

export function heatmapsToKeyPoints(hms: Array<any>): Array<Array<number>> {
  const max_coords = new Array<Array<number>>(68);
  const max_conf = new Array<number>(68);
  for (let i = 0; i < 68; i++) {
    max_coords[i] = [0, 0];
    max_conf[i] = 0;
  }

  for (let y = 0; y < 64; y++) {
    for (let x = 0; x < 64; x++) {
      for (let idx = 0; idx < 68; idx++) {
        if (hms[y][x][idx] > max_conf[idx]) {
          max_conf[idx] = hms[y][x][idx];
          max_coords[idx] = [x, y];
        }
      }
    }
  }

  for (let i = 0; i < 68; i++) {
    const xr = max_coords[i][0] / 64;
    const yr = max_coords[i][1] / 64;
    max_coords[i] = [Math.round(xr * 480 + 80), Math.round(yr * 480)];
  }

  return max_coords;
}

export const drawKeyPoints = (d: DrawContext): DrawContext => {
  let index = 0;
  if (d.kp && d.frame) {
    const flat_helper = d.frame.width * 4;
    for (let i = 0; i < 68; i++) {
      index = d.kp.points[i][1] * flat_helper + d.kp.points[i][0] * 4;
      d.frame.data[index] = 0;
      d.frame.data[index + 1] = 255;
      d.frame.data[index + 2] = 0;
      index -= 4;
      d.frame.data[index] = 0;
      d.frame.data[index + 1] = 255;
      d.frame.data[index + 2] = 0;
      index += 8;
      d.frame.data[index] = 0;
      d.frame.data[index + 1] = 255;
      d.frame.data[index + 2] = 0;
      index -= 4 * (d.frame.width + 1);
      d.frame.data[index] = 0;
      d.frame.data[index + 1] = 255;
      d.frame.data[index + 2] = 0;
      index -= 4;
      d.frame.data[index] = 0;
      d.frame.data[index + 1] = 255;
      d.frame.data[index + 2] = 0;
      index += 8;
      d.frame.data[index] = 0;
      d.frame.data[index + 1] = 255;
      d.frame.data[index + 2] = 0;
      index += 8 * (d.frame.width - 1);
      d.frame.data[index] = 0;
      d.frame.data[index + 1] = 255;
      d.frame.data[index + 2] = 0;
      index -= 4;
      d.frame.data[index] = 0;
      d.frame.data[index + 1] = 255;
      d.frame.data[index + 2] = 0;
      index += 8;
      d.frame.data[index] = 0;
      d.frame.data[index + 1] = 255;
      d.frame.data[index + 2] = 0;
    }

    d.ctx.putImageData(d.frame, 0, 0);
  }

  return d;
};

export const drawNegated = (d: DrawContext): DrawContext => {
  if (d.frame) {
    const l = d.frame.data.length / 4;

    for (let i = 0; i < l; i++) {
      const ri = i * 4 + 0;
      const gi = i * 4 + 1;
      const bi = i * 4 + 2;
      const r = d.frame.data[ri];
      const g = d.frame.data[gi];
      const b = d.frame.data[bi];
      d.frame.data[ri] = 255 - r;
      d.frame.data[gi] = 255 - g;
      d.frame.data[bi] = 255 - b;
    }

    d.ctx.putImageData(d.frame, 0, 0);
  }

  return d;
};

export const drawRedBox = (d: DrawContext): DrawContext => {
  // find and apply rotations
  d = transformFilter(d);
  if (d.kp && d.finfo && d.finfo.img && d.kp.transform) {
    // put filter in frame
    d.ctx.putImageData(
      d.finfo.img,
      d.kp.transform.left_x,
      d.kp.transform.upper_y
    );
  }

  return d;
};

export const drawShades = (d: DrawContext): DrawContext => {
  d = transformFilter(d);
  if (d.kp && d.finfo && d.finfo.img && d.kp.transform) {
    // put filter in frame
    d.ctx.putImageData(
      d.finfo.img,
      d.kp.transform.left_x,
      d.kp.transform.upper_y
    );
  }

  return d;
};

export const loadImage = async (url: string): Promise<ImageData> => {
  const img = new Image();
  img.src = url;

  // for why original version of this didn't work: https://code-examples.net/en/q/188f89a
  // fixed with: https://developer.mozilla.org/en-US/docs/Web/HTML/CORS_enabled_image
  // NOTE: you would probably never do this in a real app
  img.crossOrigin = "Anonymous";
  await img.decode();

  const tmp_canvas = document.createElement("canvas");
  tmp_canvas.width = img.width;
  tmp_canvas.height = img.height;

  let tmp_ctx = tmp_canvas.getContext("2d") as CanvasRenderingContext2D;
  tmp_ctx.drawImage(img, 0, 0);

  // this is the line that throws unless you tell CORS to shut up
  return tmp_ctx.getImageData(0, 0, img.width, img.height);
};
