import { GraphModel } from "@tensorflow/tfjs";

export interface KeyPointTransform {
  points: Array<Array<number>>;
  max_eye: number;
  max_nose: number;
  rotations?: {
    x: number; // ???
    y: number;
    zero: number; // ???
    degree: number;
  };
  transform?: {
    left_x: number;
    upper_y: number;
  };
}

export interface FilterImageInfo {
  img: ImageData | null;
  needsToLoad: boolean;
  load: { (): Promise<ImageData | null> };
}

export interface DrawContext {
  ctx: CanvasRenderingContext2D;
  img: CanvasImageSource;
  width: number;
  height: number;
  frame?: ImageData;
  kp?: KeyPointTransform;
  finfo?: FilterImageInfo; // used to store the filter currently being drawn
}

export type ImageFilterFunction = (d: DrawContext) => DrawContext;

export interface Filter {
  name: string;
  active: boolean;
  drawOrder: number;
  requiresKeyPoints: boolean;
  draw: ImageFilterFunction;
  finfo: FilterImageInfo;
}

// https://stackoverflow.com/questions/54438012/an-index-signature-parameter-type-cannot-be-a-union-type-consider-using-a-mappe
export enum TrackSets {
  local = "Local",
  remote = "Remote"
}

export interface MediaState {
  isActivelyDrawing: boolean;
  filters: Array<Filter>;
  kpmod: GraphModel | null;
  mediaTracks: { [id in TrackSets]: Set<MediaStreamTrack> };
}
