import { MediaState } from './media/state'
import { UserState } from './user/state'
import { WebSocketState } from './websocket/state'
import { WebRtcState } from './webrtc/state'

export interface State {
  media: MediaState,
  user: UserState,
  websocket: WebSocketState,
  webrtc: WebRtcState,
}