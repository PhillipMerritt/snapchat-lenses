import { UserState } from "./state";
import { getStoreAccessors } from "typesafe-vuex";
import { State } from "../state";
import { ActionContext } from "vuex";

type UserContext = ActionContext<UserState, State>;

export const actions = {};

const { dispatch } = getStoreAccessors<UserState | any, State>("");
