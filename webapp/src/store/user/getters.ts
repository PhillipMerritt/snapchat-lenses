import { UserState } from './state';
import { getStoreAccessors } from 'typesafe-vuex';
import { State } from '../state';

export const getters = {
  amLoggedIn: (state: UserState) => state.amLoggedIn,
  userName: (state: UserState) => state.userName,
  chatMessages: (state: UserState) => state.chatMessages,
  chatMessagesForUser: (state: UserState) => (remoteUser: string) => {
    if (state.chatMessages[remoteUser])
      return state.chatMessages[remoteUser];
    else return [];
  },
}

const { read } = getStoreAccessors<UserState, State>('');

export const readAmLoggedIn = read(getters.amLoggedIn);
export const readUserName = read(getters.userName);
export const readChatMessages = read(getters.chatMessages);
export const readChatMessagesForUser = read(getters.chatMessagesForUser);
