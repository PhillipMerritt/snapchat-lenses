import { mutations } from './mutations';
import { getters } from './getters';
import { actions } from './actions';
import { UserState } from './state';

const defaultUserState: UserState = {
  amLoggedIn: false,
  userName: "",
  chatMessages: {},
}

export const userModule = {
  state: defaultUserState,
  mutations,
  getters,
  actions,
}