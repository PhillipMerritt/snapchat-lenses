import { ChatMessage, UserChatMessages, UserState } from "./state";
import { getStoreAccessors } from "typesafe-vuex";
import { State } from "../state";

export const mutations = {
  setLoggedIn: (state: UserState, payload: boolean) => {
    state.amLoggedIn = payload;
  },
  setUserName: (state: UserState, payload: string) => {
    state.userName = payload;
  },
  setChatMessages: (state: UserState, payload: UserChatMessages) => {
    state.chatMessages = payload;
  },
  addChatMessage: (state: UserState, payload: ChatMessage) => {
    const remoteUser =
      payload.to !== state.userName ? payload.to : payload.from;
    if (state.chatMessages[remoteUser])
      state.chatMessages[remoteUser].push(payload);
    else state.chatMessages[remoteUser] = [payload];
  },
  logOut: (state: UserState) => {
    state.amLoggedIn = false;
    state.userName = "";
  }
};

const { commit } = getStoreAccessors<UserState | any, State>("");

export const commitSetLoggedIn = commit(mutations.setLoggedIn);
export const commitSetUserName = commit(mutations.setUserName);
export const commitSetChatMessages = commit(mutations.setChatMessages);
export const commitAddChatMessage = commit(mutations.addChatMessage);
export const commitLogOut = commit(mutations.logOut);
