// in a multi-user call, "to" would identify 
// a unique room rather than a unique user.
export interface ChatMessage {
  from: string; // name of sender
  to: string; // name of receiver
  data: string; // message content
  timestamp: number;
}

// maps remote usernames to a list of messages
// between them and the current user.
export interface UserChatMessages {
  [Key: string]: Array<ChatMessage>;
}

export interface UserState {
  amLoggedIn: boolean;
  userName: string;
  chatMessages: UserChatMessages;
}