import { WebRtcState } from "./state";
import { getStoreAccessors } from "typesafe-vuex";
import { State } from "../state";
import { ActionContext } from "vuex";
import {
  handleDataChannelClose,
  handleDataChannelError,
  handleDataChannelMessage,
  handleDataChannel,
  handleIceCandidate,
  handleIceConnectionStateChange,
  handleNegotiationNeeded
} from "./scripts/handlers";
import {
  commitSetRemoteUser,
  commitSetPeerConnection,
  commitSetDataChannel,
  commitSetLocalDescription
} from "./mutations";
import { dispatchSendWebSocketMessage } from "../websocket/actions";
import { ChatMessage } from "../user/state";
import { commitAddChatMessage } from "../user/mutations";
import { TrackSets } from "../media/state";
import { commitSetMediaTracks } from "../media/mutations";

export type WebRtcContext = ActionContext<WebRtcState, State>;

//using Google public stun server
export const defaultRtcConfiguration = {
  iceServers: [{ urls: "stun:stun2.1.google.com:19302" }]
};

export const actions = {
  async actionOpenRtcPeerConnection(
    context: WebRtcContext,
    payload: {
      conf: RTCConfiguration;
      ontrack: { (event: RTCTrackEvent): void };
    }
  ) {
    if (!context.state.peerConnection) {
      const conn = new RTCPeerConnection(payload.conf);
      conn.ontrack = payload.ontrack;
      conn.onicecandidate = handleIceCandidate(context);
      conn.onnegotiationneeded = handleNegotiationNeeded(context);
      conn.oniceconnectionstatechange = handleIceConnectionStateChange(context);
      conn.ondatachannel = handleDataChannel(context);

      for (let t of context.state.exposedTracks) {
        conn.addTrack(t);
      }

      commitSetPeerConnection(context, conn);

      const dataChannel = conn.createDataChannel("dataChannel");
      dataChannel.onerror = handleDataChannelError;
      dataChannel.onmessage = handleDataChannelMessage(context);
      dataChannel.onclose = handleDataChannelClose;

      commitSetDataChannel(context, dataChannel);
    }
  },
  async actionCloseRtcPeerConnection(context: WebRtcContext) {
    if (context.state.peerConnection) {
      await context.state.peerConnection.close();
    }
    commitSetRemoteUser(context, "");
    commitSetPeerConnection(context, undefined);
    commitSetMediaTracks(context, { source: TrackSets.remote, tracks: [] });
  },
  async actionSendDataChannelMessage(
    context: WebRtcContext,
    payload: ChatMessage
  ) {
    commitAddChatMessage(context, payload);
    if (context.state.dataChannel)
      await context.state.dataChannel.send(JSON.stringify(payload));
  },
  async actionSendWebRtcOffer(context: WebRtcContext, payload: string) {
    if (context.state.peerConnection) {
      commitSetRemoteUser(context, payload);
      const offer = await context.state.peerConnection.createOffer();
      commitSetLocalDescription(context, offer);
      await dispatchSendWebSocketMessage(context, {
        type: "offer",
        offer: offer
      });
    }
  },
  async actionHandleWebRtcOffer(
    context: WebRtcContext,
    payload: { offer: RTCSessionDescriptionInit; name: string }
  ) {
    if (context.state.peerConnection) {
      commitSetRemoteUser(context, payload.name);
      // apparently the RTCSessionDescription constructor is deprecated.
      // once i've got stuff working i'll see about updating that bit.
      context.state.peerConnection.setRemoteDescription(
        new RTCSessionDescription(payload.offer)
      );
      const answer = await context.state.peerConnection.createAnswer();
      context.state.peerConnection.setLocalDescription(answer);
      await dispatchSendWebSocketMessage(context, {
        type: "answer",
        answer: answer
      });
    }
  }
};

const { dispatch } = getStoreAccessors<WebRtcState | any, State>("");

export const dispatchOpenRtcPeerConnection = dispatch(
  actions.actionOpenRtcPeerConnection
);
export const dispatchCloseRtcPeerConnection = dispatch(
  actions.actionCloseRtcPeerConnection
);
export const dispatchSendDataChannelMessage = dispatch(
  actions.actionSendDataChannelMessage
);
export const dispatchSendWebRtcOffer = dispatch(actions.actionSendWebRtcOffer);
export const dispatchHandleWebRtcOffer = dispatch(
  actions.actionHandleWebRtcOffer
);
