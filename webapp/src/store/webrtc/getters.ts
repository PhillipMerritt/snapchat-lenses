import { WebRtcState } from './state';
import { getStoreAccessors } from 'typesafe-vuex';
import { State } from '../state';

export const getters = {
  remoteUser: (state: WebRtcState) => state.remoteUser,
  peerConnection: (state: WebRtcState) => state.peerConnection,
  dataChannel: (state: WebRtcState) => state.dataChannel,
}

const { read } = getStoreAccessors<WebRtcState, State>('');

export const readRemoteUser = read(getters.remoteUser);
export const readPeerConnection = read(getters.peerConnection);
export const readDataChannel = read(getters.dataChannel);
