import { mutations } from "./mutations";
import { getters } from "./getters";
import { actions } from "./actions";
import { WebRtcState } from "./state";

const defaultWebRtcState: WebRtcState = {
  remoteUser: "",
  peerConnection: undefined,
  dataChannel: undefined,
  exposedTracks: new Set()
};

export const webRTCModule = {
  state: defaultWebRtcState,
  mutations,
  getters,
  actions
};
