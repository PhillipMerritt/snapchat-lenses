import { WebRtcState } from "./state";
import { getStoreAccessors } from "typesafe-vuex";
import { State } from "../state";

export const mutations = {
  setRemoteUser: (state: WebRtcState, payload: string) => {
    state.remoteUser = payload;
  },
  setPeerConnection: (
    state: WebRtcState,
    payload: RTCPeerConnection | undefined
  ) => {
    state.peerConnection = payload;
  },
  setDataChannel: (state: WebRtcState, payload: RTCDataChannel | undefined) => {
    state.dataChannel = payload;
  },
  attachWebRtcMediaTrack: (state: WebRtcState, payload: MediaStreamTrack) => {
    if (state.peerConnection) {
      state.peerConnection.addTrack(payload);
    }
    state.exposedTracks.add(payload);
  },
  attachWebRtcMediaTracks: (
    state: WebRtcState,
    payload: Iterable<MediaStreamTrack>
  ) => {
    for (let t of payload) {
      if (state.peerConnection) state.peerConnection.addTrack(t);
      state.exposedTracks.add(t);
    }
  },
  setLocalDescription: (
    state: WebRtcState,
    payload: RTCSessionDescriptionInit
  ) => {
    if (state.peerConnection) state.peerConnection.setLocalDescription(payload);
  },
  setRemoteDescription: (
    state: WebRtcState,
    payload: RTCSessionDescription | RTCSessionDescriptionInit
  ) => {
    if (state.peerConnection)
      state.peerConnection.setRemoteDescription(payload);
  },
  addIceCandidate: (state: WebRtcState, payload: RTCIceCandidateInit) => {
    if (state.peerConnection)
      state.peerConnection.addIceCandidate(new RTCIceCandidate(payload));
  }
};

const { commit } = getStoreAccessors<WebRtcState | any, State>("");

export const commitSetRemoteUser = commit(mutations.setRemoteUser);
export const commitSetPeerConnection = commit(mutations.setPeerConnection);
export const commitSetDataChannel = commit(mutations.setDataChannel);
export const commitAttachWebRtcMediaTrack = commit(
  mutations.attachWebRtcMediaTrack
);
export const commitAttachWebRtcMediaTracks = commit(
  mutations.attachWebRtcMediaTracks
);
export const commitSetRemoteDescription = commit(
  mutations.setRemoteDescription
);
export const commitAddIceCandidate = commit(mutations.addIceCandidate);
export const commitSetLocalDescription = commit(mutations.setLocalDescription);
