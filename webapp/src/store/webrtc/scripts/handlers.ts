import { Store } from "vuex";
import { commitAddChatMessage } from "../../user/mutations";
import {
  commitSetRemoteDescription,
  commitAddIceCandidate,
  commitSetDataChannel
} from "../mutations";
import {
  WebRtcContext,
  dispatchHandleWebRtcOffer,
  dispatchSendWebRtcOffer,
  dispatchCloseRtcPeerConnection
} from "../actions";
import { dispatchSendWebSocketMessage } from "../../websocket/actions";

// default websocket message handler for webrtc,
// can be called by components or whatever else
// has its own login and logout handlers.
export const webRtcSocketHandler = (
  store: Store<any>,
  login: { (success: boolean): void },
  leave: { (): void }
) => async (msg: MessageEvent) => {
  // await dispatchWebSocketMessageHandler
  console.log("Got message", msg.data);

  const data = JSON.parse(msg.data);

  switch (data.type) {
    case "login":
      await login(data.success);
      break;
    // when somebody wants to call us
    case "offer":
      await dispatchHandleWebRtcOffer(store, {
        offer: data.offer,
        name: data.name
      });
      break;
    case "answer":
      commitSetRemoteDescription(store, data.answer);
      break;
    // when a remote peer sends an ice candidate to us
    case "candidate":
      commitAddIceCandidate(store, data.candidate);
      break;
    case "leave":
      await leave();
      break;
    default:
      break;
  }
};

export const handleIceCandidate = (context: WebRtcContext) => async (
  event: RTCPeerConnectionIceEvent
) => {
  if (event.candidate) {
    await dispatchSendWebSocketMessage(context, {
      type: "candidate",
      candidate: event.candidate
    });
  }
};

export const handleNegotiationNeeded = (context: WebRtcContext) => async (
  event: Event
) => {
  // allows us to activate local video after starting
  // the call. don't send offer until we have someone to
  // send it to. handler based on example from docs:
  // https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/negotiationneeded_event
  if (context.state.remoteUser)
    await dispatchSendWebRtcOffer(context, context.state.remoteUser);
};

// https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/iceConnectionState
export const handleIceConnectionStateChange = (
  context: WebRtcContext
) => async (event: Event) => {
  if (context.state.peerConnection) {
    switch (context.state.peerConnection.iceConnectionState) {
      // one or more transports has terminated unexpectedly
      // or in an error, or else the connection has been closed
      case "disconnected":
      case "failed":
      case "closed":
        await dispatchCloseRtcPeerConnection(context);
        break;
    }
  }
};

export const handleDataChannel = (context: WebRtcContext) => (
  event: RTCDataChannelEvent
) => {
  event.channel.onerror = handleDataChannelError;
  event.channel.onmessage = handleDataChannelMessage(context);
  event.channel.onclose = handleDataChannelClose;

  commitSetDataChannel(context, event.channel);
};

export const handleDataChannelError = (error: RTCErrorEvent) => {
  console.log("data channel error: ", error);
};

export const handleDataChannelMessage = (context: WebRtcContext) => (
  event: MessageEvent
) => {
  commitAddChatMessage(context, JSON.parse(event.data));
};

export const handleDataChannelClose = () => {
  console.log("data channel is closed");
};
