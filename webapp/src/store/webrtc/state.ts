export interface WebRtcState {
  remoteUser: string;
  peerConnection?: RTCPeerConnection;
  dataChannel?: RTCDataChannel;
  exposedTracks: Set<MediaStreamTrack>;
}
