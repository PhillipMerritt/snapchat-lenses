import { WebSocketState, WebSocketMessage } from "./state";
import { commitSetWs } from "./mutations";
import { getStoreAccessors } from "typesafe-vuex";
import { State } from "../state";
import { ActionContext } from "vuex";

type WebSocketContext = ActionContext<WebSocketState, State>;

export const actions = {
  async actionOpenWebSocketConnection(
    context: WebSocketContext,
    payload: { (event: MessageEvent): void }
  ) {
    if (!context.state.ws) {
      const client_id = Date.now();

      const wsUrl = `ws://localhost:8000/webrtc/${client_id}`;

      const ws = await new WebSocket(wsUrl);

      ws.onopen = async function() {
        console.log("Connected to the signaling server");
      };

      ws.onerror = async function(err) {
        console.log("Got error", err);
      };

      ws.onmessage = payload;

      commitSetWs(context, ws);
    }
  },

  async actionCloseWebSocketConnection(context: WebSocketContext) {
    if (context.state.ws) await context.state.ws.close();
    commitSetWs(context, undefined);
  },

  async actionSendWebSocketMessage(
    context: WebSocketContext,
    payload: WebSocketMessage
  ) {
    const ru = context.rootState.webrtc.remoteUser;
    if (ru && !payload.name) payload.name = ru;
    if (context.state.ws) await context.state.ws.send(JSON.stringify(payload));
  }
};

const { dispatch } = getStoreAccessors<WebSocketState | any, State>("");

export const dispatchOpenWebSocketConnection = dispatch(
  actions.actionOpenWebSocketConnection
);
export const dispatchCloseWebSocketConnection = dispatch(
  actions.actionCloseWebSocketConnection
);
export const dispatchSendWebSocketMessage = dispatch(
  actions.actionSendWebSocketMessage
);
