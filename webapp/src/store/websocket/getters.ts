import { WebSocketState } from './state';
import { getStoreAccessors } from 'typesafe-vuex';
import { State } from '../state';

export const getters = {
  ws: (state: WebSocketState) => state.ws,
}

const { read } = getStoreAccessors<WebSocketState, State>('');

export const readWs = read(getters.ws);
