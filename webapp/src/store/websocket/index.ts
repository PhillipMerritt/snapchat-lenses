import { mutations } from './mutations';
import { getters } from './getters';
import { actions } from './actions';
import { WebSocketState } from './state';

const defaultWebSocketState: WebSocketState = {
  ws: undefined
}

export const webSocketModule = {
  state: defaultWebSocketState,
  mutations,
  getters,
  actions,
}