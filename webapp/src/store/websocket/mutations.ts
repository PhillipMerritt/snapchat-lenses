import { WebSocketState } from './state';
import { getStoreAccessors } from 'typesafe-vuex';
import { State } from '../state';

export const mutations = {
  setWs: (state: WebSocketState, payload: WebSocket | undefined) => {
    state.ws = payload;
  },
}

const { commit } = getStoreAccessors<WebSocketState | any, State>('');

export const commitSetWs = commit(mutations.setWs);