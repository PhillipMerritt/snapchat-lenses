export interface LoginRequest {
  type: "login"
  name: string
}

export interface LeaveRequest {
  type: "leave"
  name?: string
}

export interface WebRtcOffer {
  type: "offer"
  name?: string
  offer: RTCSessionDescriptionInit
}

export interface WebRtcAnswer {
  type: "answer"
  name?: string
  answer: RTCSessionDescriptionInit
}

export interface WebRtcIceCandidate {
  type: "candidate"
  name?: string
  candidate: RTCIceCandidate
}

export type WebRtcSignalingMessage = WebRtcOffer | WebRtcAnswer | WebRtcIceCandidate;

export type WebSocketMessage = LoginRequest | LeaveRequest | WebRtcSignalingMessage;

export interface WebSocketState {
  ws?: WebSocket,
}